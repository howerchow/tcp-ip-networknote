#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
void error_handling( char* message );

int main( int argc, char* argv[] )
{
    if ( argc != 3 )
    {
        printf( "Usage : %s <IP> <port>\n", argv[ 0 ] );
        exit( EXIT_FAILURE );
    }

    int sock;
    struct sockaddr_in send_adr;

    sock = socket( AF_INET, SOCK_STREAM, 0 );

    memset( &send_adr, 0, sizeof( send_adr ) );
    send_adr.sin_family = AF_INET;
    inet_pton( AF_INET, argv[ 1 ], &send_adr.sin_addr );
    send_adr.sin_port = htons( atoi( argv[ 2 ] ) );

    if ( connect( sock, (struct sockaddr*)&send_adr, sizeof( send_adr ) ) == -1 )
        error_handling( "connect() error" );

    write( sock, "123", strlen( "123" ) );
    close( sock );
    return 0;
}

void error_handling( char* message )
{
    fputs( message, stderr );
    fputc( '\n', stderr );
    exit( EXIT_FAILURE );
}
