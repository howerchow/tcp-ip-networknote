#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>

int main( int argc, char* argv[] )
{
    struct sockaddr_in addr = {};
    addr.sin_addr.s_addr = htonl( 0x1020304 );
    char str_arr[ 20 ];
    // 把addr1中的结构体信息转换为字符串的IP地址形式
    //  输出： Dotted-Decimal notation1: 1.2.3.4

    if ( inet_ntop( AF_INET, &addr.sin_addr, str_arr, INET_ADDRSTRLEN ) != NULL )
    {
        printf( "Dotted-Decimal notation1: %s \n", str_arr );
    }

    return 0;
}
