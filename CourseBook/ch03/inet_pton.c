#include <arpa/inet.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>

int main( int argc, char* argv[] )
{
    const char* ipaddr = "1.2.3.4";

    struct sockaddr_in addr = {};
    addr.sin_addr.s_addr = htonl( 0x1020304 );

    // 把字符串的IP地址形式ipaddr转换为addr中的结构体信息
    //  输出：Network ordered integer addr: 0x4030201

    if ( inet_pton( AF_INET, ipaddr, &addr.sin_addr ) != -1 )
    {
        printf( "Network ordered integer addr: %#" PRIx32 "\n", addr.sin_addr.s_addr );
    }

    return 0;
}
