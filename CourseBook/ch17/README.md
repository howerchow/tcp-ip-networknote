## 第 17 章 优于 select 的 epoll

### 17.1 epoll 理解及应用

#### 17.1.3 实现 epoll 时必要的函数和结构体

能够克服 select 函数缺点的 epoll 函数具有以下优点，这些优点正好与之前的 select 函数缺点相反。

- 无需编写以监视状态变化为目的的针对所有文件描述符的循环语句
- 调用对应于 select 函数的 epoll_wait 函数时无需每次传递监视对象信息。

下面是 epoll 函数的功能：

```c
       #include <sys/epoll.h>

	   // 创建保存 epoll 文件描述符的空间
       int epoll_create(int size);
       int epoll_create1(int flags);
```

```c
       #include <sys/epoll.h>

	   // 向空间注册并注销文件描述符
       int epoll_ctl(int epfd, int op, int fd,
                     struct epoll_event *_Nullable event);
```

```c
       #include <sys/epoll.h>

	   //与 select 函数类似，等待文件描述符发生变化
       int epoll_wait(int epfd, struct epoll_event *events,
                      int maxevents, int timeout);
       int epoll_pwait(int epfd, struct epoll_event *events,
                      int maxevents, int timeout,
                      const sigset_t *_Nullable sigmask);
       int epoll_pwait2(int epfd, struct epoll_event *events,
                      int maxevents, const struct timespec *_Nullable timeout,
                      const sigset_t *_Nullable sigmask);

```

```c
	struct epoll_event
	{
		__uint32_t events;
		epoll_data_t data;
	};

	typedef union epoll_data {
		void *ptr;
		int fd;
		__uint32_t u32;
		__uint64_t u64;
	} epoll_data_t;

```

#### 17.1.4 epoll_create

```c
#include <sys/epoll.h>
int epoll_create(int size);
/*
成功时返回 epoll 的文件描述符，失败时返回 -1
size：epoll 实例的大小
*/
```

#### 17.1.5 epoll_ctl


```c
#include <sys/epoll.h>
int epoll_ctl(int epfd, int op, int fd, struct epoll_event *event);
/*
成功时返回 0 ，失败时返回 -1
epfd：用于注册监视对象的 epoll 例程的文件描述符
op：用于指定监视对象的添加、删除或更改等操作
fd：需要注册的监视对象文件描述符
event：监视对象的事件类型
*/
```

```c
epoll_ctl(efd, EPOLL_CTL_ADD, fd, C);
```

```c
epoll_ctl(efd, EPOLL_CTL_DEL, fd, NULL); // 从监视对象中删除时，不需要监视类型，因此向第四个参数可以传递为 NULL
```

下面是第二个参数的含义：

- EPOLL_CTL_ADD：将文件描述符注册到 epoll 例程
- EPOLL_CTL_DEL：从 epoll 例程中删除文件描述符
- EPOLL_CTL_MOD：更改注册的文件描述符的关注事件发生情况

epoll_event 结构体用于保存事件的文件描述符结合。但也可以在 epoll 例程中注册文件描述符时，用于注册关注的事件。该函数中 epoll_event 结构体的定义并不显眼，因此通过调用语句说明该结构体在 epoll_ctl 函数中的应用。

```c
struct epoll_event event;
...
event.events = EPOLLIN; //发生需要读取数据的情况时
event.data.fd = sockfd;
epoll_ctl(epfd, EPOLL_CTL_ADD, sockfd, &event);
...
```

上述代码将 epfd 注册到 epoll 例程 epfd 中，并在需要读取数据的情况下产生相应事件。接下来给出 epoll_event 的成员 events 中可以保存的常量及所指的事件类型。

- EPOLLIN：需要读取数据的情况
- EPOLLOUT：输出缓冲为空，可以立即发送数据的情况
- EPOLLPRI：收到 OOB 数据的情况
- EPOLLRDHUP：断开连接或半关闭的情况，这在边缘触发方式下非常有用
- EPOLLERR：发生错误的情况
- EPOLLET：以边缘触发的方式得到事件通知
- EPOLLONESHOT：发生一次事件后，相应文件描述符不再收到事件通知。因此需要向 epoll_ctl 函数的第二个参数传递 EPOLL_CTL_MOD ，再次设置事件。

可通过位或运算同时传递多个上述参数。

#### 17.1.6 epoll_wait

下面是函数原型：

```c
#include <sys/epoll.h>
int epoll_wait(int epfd, struct epoll_event *events, int maxevents, int timeout);
/*
  * 成功时返回发生事件的文件描述符个数，失败时返回 -1
  * epfd : 表示事件发生监视范围的 epoll 例程的文件描述符
  * events : 保存发生事件的文件描述符集合的结构体地址值
  * maxevents : 第二个参数中可以保存的最大事件数
  * timeout : 以 1/1000 秒为单位的等待时间，传递 -1 时，一直等待直到发生事件
  *
*/
```

该函数调用方式如下。需要注意的是，第二个参数所指缓冲需要动态分配。

```c
int event_cnt;
struct epoll_event *ep_events;
...
ep_events = malloc(sizeof(struct epoll_event) * EPOLL_SIZE);//EPOLL_SIZE是宏常量
...
event_cnt = epoll_wait(epfd,ep_events,EPOLL_SIZE,-1);
...
```

#### 17.1.7 基于 epoll 的回声服务器端

epoll 的流程：

1.  epoll_create 创建一个保存 epoll 文件描述符的空间，可以没有参数
2. 动态分配内存，给将要监视的 epoll_wait
3. 利用 epoll_ctl 控制 添加 删除，监听事件
4. 利用 epoll_wait 来获取改变的文件描述符,来执行程序

### 17.2 水平触发和边缘触发

学习 epoll 时要了解水平触发（Level Trigger）和边缘触发（Edge Trigger）

#### 17.2.1 水平触发和边缘触发的区别在于发生事件的时间点

**水平触发的特性**：

> 水平触发方式中，只要输入缓冲有数据就会一直通知该事件

例如，服务器端输入缓冲收到 50 字节数据时，服务器端操作系统将通知该事件（注册到发生变化的文件描述符）。但是服务器端读取 20 字节后还剩下 30 字节的情况下，仍会注册事件。也就是说，水平触发方式中，只要输入缓冲中还剩有数据，就将以事件方式再次注册。

**边缘触发特性**：

边缘触发中输入缓冲收到数据时仅注册 1 次该事件。即使输入缓冲中还留有数据，也不会再进行注册。

#### 17.2.2 掌握水平触发的事件特性

下面代码修改自 [echo_epollserv.c](https://github.com/riba2534/TCP-IP-NetworkNote/blob/master/ch17/echo_epollserv.c) 。epoll 默认以水平触发的方式工作，因此可以通过该示例验证水平触发的特性。

- [echo_EPLTserv.c](https://github.com/riba2534/TCP-IP-NetworkNote/blob/master/ch17/echo_EPLTserv.c)

上面的代码把调用 read 函数时使用的缓冲大小缩小到了 4 个字节，插入了验证 epoll_wait 调用次数的验证函数。减少缓冲大小是为了阻止服务器端一次性读取接收的数据。换言之，调用 read 函数后，输入缓冲中仍有数据要读取，而且会因此注册新的事件并从 epoll_wait 函数返回时将循环输出「return epoll_wait」字符串。

编译运行:

```shell
gcc echo_EPLTserv.c -o serv
./serv 9190
```

运行结果：

![](https://i.loli.net/2019/02/01/5c540825ae415.png)

从结果可以看出，每当收到客户端数据时，都会注册该事件，并因此调用 epoll_wait 函数。

下面的代码是修改后的边缘触发方式的代码，仅仅是把上面的代码改为：

```c
 event.events = EPOLLIN | EPOLLET;
```

代码：

- [echo_EDGEserv.c](https://github.com/riba2534/TCP-IP-NetworkNote/blob/master/ch17/echo_EDGEserv.c)

编译运行：

```shell
gcc echo_EDGEserv.c -o serv
./serv 9190
```

结果：

![](https://i.loli.net/2019/02/01/5c54097b6469f.png)

从上面的例子看出，接收到客户端的消息时，只输出一次「return epoll_wait」字符串，这证明仅注册了一次事件。

**select 模型是以水平触发的方式工作的**。

#### 17.2.3 边缘触发的服务器端必知的两点

- 通过 errno 变量验证错误原因
- 为了完成非阻塞（Non-blocking）I/O ，更改了套接字特性。

Linux 套接字相关函数一般通过 -1 通知发生了错误。虽然知道发生了错误，但仅凭这些内容无法得知产生错误的原因。因此，为了在发生错误的时候提额外的信息，Linux 声明了如下全局变量：

```c
int errno;
```

为了访问该变量，需要引入 `error.h` 头文件，因此此头文件有上述变量的 extren 声明。另外，每种函数发生错误时，保存在 errno 变量中的值都不同。

> read 函数发现输入缓冲中没有数据可读时返回 -1，同时在 errno 中保存 EAGAIN 常量

下面是 Linux 中提供的改变和更改文件属性的办法：

```c
#include <fcntl.h>
int fcntl(int fields, int cmd, ...);
/*
成功时返回 cmd 参数相关值，失败时返回 -1
filedes : 属性更改目标的文件描述符
cmd : 表示函数调用目的
*/
```

从上述声明可以看出 fcntl 有可变参数的形式。如果向第二个参数传递 F_GETFL ，可以获得第一个参数所指的文件描述符属性（int 型）。反之，如果传递 F_SETFL ，可以更改文件描述符属性。若希望将文件（套接字）改为非阻塞模式，需要如下  2 条语句。

```C
int flag = fcntl(fd,F_GETFL,0);
fcntl(fd, F_SETFL, flag | O_NONBLOCK);
```

通过第一条语句，获取之前设置的属性信息，通过第二条语句在此基础上添加非阻塞 O_NONBLOCK 标志。调用 read/write 函数时，无论是否存在数据，都会形成非阻塞文件（套接字）。fcntl 函数的适用范围很广。

#### 17.2.4 实现边缘触发回声服务器端

通过 errno 确认错误的原因是：边缘触发方式中，接收数据仅注册一次该事件。

因为这种特点，一旦发生输入相关事件时，就应该读取输入缓冲中的全部数据。因此需要验证输入缓冲是否为空。

> read 函数返回 -1，变量 errno 中的值变成 EAGAIN 时，说明没有数据可读。

既然如此，为什么要将套接字变成非阻塞模式？边缘触发条件下，以阻塞方式工作的 read & write 函数有可能引起服务端的长时间停顿。因此，边缘触发方式中一定要采用非阻塞 read & write 函数。

下面是以边缘触发方式工作的回声服务端代码：

- [echo_EPETserv.c](https://github.com/riba2534/TCP-IP-NetworkNote/blob/master/ch17/echo_EPETserv.c)

编译运行：

```shell
gcc echo_EPETserv.c -o serv
./serv
```

结果：

![](https://i.loli.net/2019/02/01/5c542149c0cee.png)

#### 17.2.5 水平触发和边缘触发孰优孰劣

边缘触发方式可以做到这点：

> 可以分离接收数据和处理数据的时间点！

下面是边缘触发的图

![](https://i.loli.net/2019/02/01/5c5421e3b3f2b.png)

运行流程如下：

- 服务器端分别从 A B C 接收数据
- 服务器端按照  A B C 的顺序重新组合接收到的数据
- 组合的数据将发送给任意主机。

为了完成这个过程，如果可以按照如下流程运行，服务端的实现并不难：

- 客户端按照 A B C 的顺序连接服务器，并且按照次序向服务器发送数据
- 需要接收数据的客户端应在客户端 A B C 之前连接到服务器端并等待

但是实际情况中可能是下面这样：

- 客户端 C 和 B 正在向服务器发送数据，但是 A 并没有连接到服务器
- 客户端 A B C 乱序发送数据
- 服务端已经接收到数据，但是要接收数据的目标客户端并没有连接到服务器端。

因此，即使输入缓冲收到数据，服务器端也能决定读取和处理这些数据的时间点，这样就给服务器端的实现带来很大灵活性。

### 17.3 习题


5. epoll 是以水平触发和边缘触发方式工作。二者有何差别？从输入缓冲的角度说明这两种方式通知事件的时间点差异。

   答：在水平触发中，只要输入缓冲有数据，就会一直通知该事件。边缘触发中输入缓冲收到数据时仅注册 1 次该事件，即使输入缓冲中还留有数据，也不会再进行注册。

6. 采用边缘触发时可以分离数据的接收和处理时间点。请说明其优点和原因。

   答：分离接收数据和处理数据的时间点，给服务端的实现带来很大灵活性。
