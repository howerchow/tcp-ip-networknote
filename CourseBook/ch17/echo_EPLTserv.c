#define _GNU_SOURCE
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <unistd.h>

#define BUF_SIZE 1024
#define EPOLL_SIZE 50

void error_handling( char* message );
int setnonblocking( int fd );

int main( int argc, char* argv[] )
{
    if ( argc != 2 )
    {
        printf( "Usage : %s <port> \n", argv[ 0 ] );
        exit( EXIT_FAILURE );
    }

    int serv_sock, clnt_sock;
    struct sockaddr_in serv_adr, clnt_adr;
    socklen_t adr_sz;
    int n;
    char buffer[ BUF_SIZE ] = {};

    int epfd, nfds;

    serv_sock = socket( AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0 );
    if ( serv_sock == -1 )
    {
        perror( "socket() error! %s" );
        exit( EXIT_FAILURE );
    }

    int one = 1;
    /* REUSEADDR on Unix means, "don't hang on to this address after the
     * listener is closed."  On Windows, though, it means "don't keep other
     * processes from binding to this address while we're using it. */
    setsockopt( serv_sock, SOL_SOCKET, SO_REUSEADDR, (void*)&one, sizeof( one ) );
    /* REUSEPORT on Linux 3.9+ means, "Multiple servers (processes or
     * threads) can bind to the same port if they each set the option. */
    setsockopt( serv_sock, SOL_SOCKET, SO_REUSEPORT, (void*)&one, sizeof( one ) );

    memset( &serv_adr, 0, sizeof( serv_adr ) );
    serv_adr.sin_family = AF_INET;
    serv_adr.sin_addr.s_addr = htonl( INADDR_ANY );
    serv_adr.sin_port = htons( atoi( argv[ 1 ] ) );

    if ( bind( serv_sock, (struct sockaddr*)&serv_adr, sizeof( serv_adr ) ) == -1 )
        error_handling( "bind() error" );

    if ( listen( serv_sock, SOMAXCONN ) == -1 )
        error_handling( "listen() error" );

    epfd = epoll_create( EPOLL_SIZE );
    struct epoll_event* ep_events = malloc( sizeof( struct epoll_event ) * EPOLL_SIZE );
    if ( ep_events == NULL )
    {
        exit( EXIT_FAILURE );
    }
    memset( ep_events, '\0', sizeof( struct epoll_event ) * EPOLL_SIZE );

    struct epoll_event event;
    event.events = EPOLLIN;
    event.data.fd = serv_sock;
    epoll_ctl( epfd, EPOLL_CTL_ADD, serv_sock, &event );

    for ( ;; )
    {
        nfds = epoll_wait( epfd, ep_events, EPOLL_SIZE, 1000 );
        if ( nfds == -1 )
        {
            if ( errno == EINTR )
            { /* interrupt */
                continue;
            }
            else
            { /* error */
                puts( "epoll_wait() error" );
                break;
            }
        }
        if ( nfds == 0 )
        { /* timeout */
            continue;
        }

        // puts("return epoll_wait");
        for ( size_t i = 0; i < nfds; ++i )
        {
            if ( ep_events[ i ].events & EPOLLIN ) // READ
            {
                if ( ep_events[ i ].data.fd == serv_sock ) // 客户端请求连接时
                {
                    adr_sz = sizeof( clnt_adr );
                    clnt_sock = accept( serv_sock, (struct sockaddr*)&clnt_adr, &adr_sz );
                    if ( clnt_sock == -1 )
                    {
                        printf( "accept(): " );
                        exit( EXIT_FAILURE );
                    }
                    setnonblocking( clnt_sock );
                    // ev.events = EPOLLIN | EPOLLOUT | EPOLLET;
                    event.events = EPOLLIN;
                    event.data.fd = clnt_sock; // 把客户端套接字添加进去
                    epoll_ctl( epfd, EPOLL_CTL_ADD, clnt_sock, &event );
                    printf( "connected client : %d \n", clnt_sock );
                }
                else if ( ep_events[ i ].events & EPOLLIN ) // 是客户端套接字时
                {
                    n = recv( ep_events[ i ].data.fd, buffer, sizeof( buffer ), 0 );
                    if ( n == -1 )
                    {
                        if ( errno != EWOULDBLOCK && errno != EINTR )
                        { /*error*/
                            epoll_ctl( epfd, EPOLL_CTL_DEL, ep_events[ i ].data.fd, NULL );
                            close( ep_events[ i ].data.fd );
                            exit( EXIT_FAILURE );
                        }
                    }
                    else if ( n == 0 )
                    { /*client shutdow*/
                        epoll_ctl( epfd, EPOLL_CTL_DEL, ep_events[ i ].data.fd, NULL );
                        close( ep_events[ i ].data.fd );
                        printf( "closed client : %d \n", ep_events[ i ].data.fd );
                    }
                    else if ( n > 0 )
                    {
                        buffer[ n ] = '\0';
                        printf( "receive message from client %d bytes: %s\n", n, buffer );
                        if ( send( ep_events[ i ].data.fd, buffer, n, 0 ) == -1 )
                        {
                            perror( "send()" );
                            exit( EXIT_FAILURE );
                        }
                    }
                }
            }
            else if ( ep_events[ i ].events & EPOLLOUT ) // write
            {
            }
            else if ( ( ep_events[ i ].events & EPOLLERR ) || ( ep_events[ i ].events & EPOLLHUP ) )
            {
                fprintf( stderr, "epoll error\n" );
                close( ep_events[ i ].data.fd );
                continue;
            }
        }
    }
    close( serv_sock );
    close( clnt_sock );
    return 0;
}

void error_handling( char* message )
{
    fputs( message, stderr );
    fputc( '\n', stderr );
    exit( EXIT_FAILURE );
}

int setnonblocking( int fd )
{
#ifdef _WIN32
    {
        unsigned long nonblocking = 1;
        if ( ioctlsocket( fd, FIONBIO, &nonblocking ) == SOCKET_ERROR )
        {
            event_sock_warn( fd, "ioctlsocket(%d, FIONBIO, &%lu)", (int)fd, (unsigned long)nonblocking );
            return -1;
        }
    }
#else
    {
        int flags;
        if ( ( flags = fcntl( fd, F_GETFL, NULL ) ) < 0 )
        {
            fprintf( stderr, "fcntl(%d, F_GETFL)\n", fd );
            return -1;
        }
        if ( !( flags & O_NONBLOCK ) )
        {
            if ( fcntl( fd, F_SETFL, flags | O_NONBLOCK ) == -1 )
            {
                fprintf( stderr, "fcntl(%d, F_SETFL)", fd );
                return -1;
            }
        }
    }
#endif
    return 0;
}
