#define _GNU_SOURCE
#include <arpa/inet.h>
#include <asm-generic/errno.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define BUF_SIZE 4 // 缓冲区设置为 4 字节
#define EPOLL_SIZE 50

void error_handling( char* message );
int setnonblocking( int fd );

int main( int argc, char* argv[] )
{
    if ( argc != 2 )
    {
        printf( "Usage : %s <port> \n", argv[ 0 ] );
        exit( EXIT_FAILURE );
    }

    int serv_sock, clnt_sock;
    struct sockaddr_in serv_adr, clnt_adr;
    socklen_t adr_sz;
    int str_len, i;
    char buf[ BUF_SIZE ] = {};

    struct epoll_event* ep_events;
    struct epoll_event event;
    int epfd, event_cnt;

    serv_sock = socket( AF_INET, SOCK_STREAM, 0 );

    int one = 1;
    /* REUSEADDR on Unix means, "don't hang on to this address after the
     * listener is closed."  On Windows, though, it means "don't keep other
     * processes from binding to this address while we're using it. */
    setsockopt( serv_sock, SOL_SOCKET, SO_REUSEADDR, (void*)&one, sizeof( one ) );
    /* REUSEPORT on Linux 3.9+ means, "Multiple servers (processes or
     * threads) can bind to the same port if they each set the option. */
    setsockopt( serv_sock, SOL_SOCKET, SO_REUSEPORT, (void*)&one, sizeof( one ) );

    memset( &serv_adr, 0, sizeof( serv_adr ) );
    serv_adr.sin_family = AF_INET;
    serv_adr.sin_addr.s_addr = htonl( INADDR_ANY );
    serv_adr.sin_port = htons( atoi( argv[ 1 ] ) );

    if ( bind( serv_sock, (struct sockaddr*)&serv_adr, sizeof( serv_adr ) ) == -1 )
        error_handling( "bind() error" );

    if ( listen( serv_sock, SOMAXCONN ) == -1 )
        error_handling( "listen() error" );

    epfd = epoll_create( EPOLL_SIZE );
    ep_events = malloc( sizeof( struct epoll_event ) * EPOLL_SIZE );
    if ( ep_events == NULL )
    {
        exit( EXIT_FAILURE );
    }
    setnonblocking( serv_sock );
    event.events = EPOLLIN;
    event.data.fd = serv_sock;
    epoll_ctl( epfd, EPOLL_CTL_ADD, serv_sock, &event );

    for ( ;; )
    {
        event_cnt = epoll_wait( epfd, ep_events, EPOLL_SIZE, -1 );
        if ( event_cnt == -1 )
        {
            puts( "epoll_wait() error" );
            break;
        }

        puts( "return epoll_wait" );
        for ( size_t i = 0; i < event_cnt; ++i )
        {
            if ( ep_events[ i ].data.fd == serv_sock )
            {
                adr_sz = sizeof( clnt_adr );
                clnt_sock = accept( serv_sock, (struct sockaddr*)&clnt_adr, &adr_sz );
                setnonblocking( clnt_sock );
                event.events = EPOLLIN | EPOLLET; // 边缘模式
                event.data.fd = clnt_sock;
                epoll_ctl( epfd, EPOLL_CTL_ADD, clnt_sock, &event );
                printf( "connected client : %d \n", clnt_sock );
            }
            else if ( ep_events[ i ].events & EPOLLIN )
            { // 边缘模式: 一次性收完数据, 循环到recv出错，errno为EWOULDBLOCK
                for ( ;; )
                {
                    str_len = recv( ep_events[ i ].data.fd, buf, BUF_SIZE, 0 );
                    if ( str_len < 0 )
                    {
                        if ( errno == EWOULDBLOCK )
                        {
                            break;
                        }
                        continue;
                    }
                    else if ( str_len == 0 )
                    {
                        epoll_ctl( epfd, EPOLL_CTL_DEL, ep_events[ i ].data.fd, NULL );
                        close( ep_events[ i ].data.fd );
                        printf( "closed client : %d \n", ep_events[ i ].data.fd );
                        break;
                    }
                    else
                    {
                        printf( "str_len: %d\n", str_len );
                        send( ep_events[ i ].data.fd, buf, str_len, 0 );
                    }
                }
            }
        }
    }
    close( serv_sock );
    close( epfd );

    return 0;
}

void error_handling( char* message )
{
    fputs( message, stderr );
    fputc( '\n', stderr );
    exit( EXIT_FAILURE );
}

void setnonblockingmode( int fd )
{
    int flag = fcntl( fd, F_GETFL, 0 );
    fcntl( fd, F_SETFL, flag | O_NONBLOCK );
}

int setnonblocking( int fd )
{
#ifdef _WIN32
    {
        unsigned long nonblocking = 1;
        if ( ioctlsocket( fd, FIONBIO, &nonblocking ) == SOCKET_ERROR )
        {
            event_sock_warn( fd, "ioctlsocket(%d, FIONBIO, &%lu)", (int)fd, (unsigned long)nonblocking );
            return -1;
        }
    }
#else
    {
        int flags;
        if ( ( flags = fcntl( fd, F_GETFL, NULL ) ) < 0 )
        {
            fprintf( stderr, "fcntl(%d, F_GETFL)\n", fd );
            return -1;
        }
        if ( !( flags & O_NONBLOCK ) )
        {
            if ( fcntl( fd, F_SETFL, flags | O_NONBLOCK ) == -1 )
            {
                fprintf( stderr, "fcntl(%d, F_SETFL)", fd );
                return -1;
            }
        }
    }
#endif
    return 0;
}
