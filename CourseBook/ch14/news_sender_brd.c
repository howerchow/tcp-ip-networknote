#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define BUF_SIZE 30
void error_handling( char* message );

int main( int argc, char* argv[] )
{
    if ( argc != 3 )
    {
        printf( "Usage : %s <GroupIP> <PORT>\n", argv[ 0 ] );
        exit( EXIT_FAILURE );
    }

    int send_sock;
    struct sockaddr_in broad_adr;
    FILE* fp;
    char buf[ BUF_SIZE ] = {};
    int so_brd = 1;

    send_sock = socket( AF_INET, SOCK_DGRAM, 0 ); // 创建  UDP 套接字

    memset( &broad_adr, 0, sizeof( broad_adr ) );
    broad_adr.sin_family = AF_INET;
    inet_pton( AF_INET, argv[ 1 ], &broad_adr.sin_addr );
    broad_adr.sin_port = htons( atoi( argv[ 2 ] ) );

    setsockopt( send_sock, SOL_SOCKET, SO_BROADCAST, (void*)&so_brd, sizeof( so_brd ) );

    if ( ( fp = fopen( "news.txt", "r" ) ) == NULL )
        error_handling( "fopen() error" );

    while ( !feof( fp ) ) // 如果文件没结束就返回0
    {
        fgets( buf, BUF_SIZE, fp );
        sendto( send_sock, buf, strlen( buf ), 0, (struct sockaddr*)&broad_adr, sizeof( broad_adr ) );
        sleep( 2 );
    }
    fclose( fp );
    close( send_sock );
    return 0;
}

void error_handling( char* message )
{
    fputs( message, stderr );
    fputc( '\n', stderr );
    exit( EXIT_FAILURE );
}
