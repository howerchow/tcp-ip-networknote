#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define TTL 64
#define BUF_SIZE 30
void error_handling( char* message );

int main( int argc, char* argv[] )
{
    if ( argc != 3 )
    {
        printf( "Usage : %s <GroupIP> <PORT>\n", argv[ 0 ] );
        exit( EXIT_FAILURE );
    }

    int send_sock;
    struct sockaddr_in mul_adr;
    int time_live = TTL;
    FILE* fp;
    char buf[ BUF_SIZE ] = {};

    send_sock = socket( AF_INET, SOCK_DGRAM, 0 ); // 创建  UDP 套接字

    memset( &mul_adr, 0, sizeof( mul_adr ) );
    mul_adr.sin_family = AF_INET;
    /* 必须将IP地址设置为多播地址 */
    inet_pton( AF_INET, argv[ 1 ], &mul_adr.sin_addr );
    mul_adr.sin_port = htons( atoi( argv[ 2 ] ) );

    // 指定套接字中 TTL 的信息
    setsockopt( send_sock, IPPROTO_IP, IP_MULTICAST_TTL, (void*)&time_live, sizeof( time_live ) );
    if ( ( fp = fopen( "news.txt", "r" ) ) == NULL )
        error_handling( "fopen() error" );

    while ( !feof( fp ) ) // 如果文件没结束就返回0
    {
        fgets( buf, BUF_SIZE, fp );
        sendto( send_sock, buf, strlen( buf ), 0, (struct sockaddr*)&mul_adr, sizeof( mul_adr ) );
        sleep( 2 );
    }

    fclose( fp );
    close( send_sock );
    return 0;
}

void error_handling( char* message )
{
    fputs( message, stderr );
    fputc( '\n', stderr );
    exit( EXIT_FAILURE );
}
