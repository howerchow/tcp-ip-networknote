#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
void error_handling( char* message );

int main( int argc, char* argv[] )
{
    int sock;
    int snd_buf, rcv_buf, state;
    socklen_t len;

    sock = socket( AF_INET, SOCK_STREAM, 0 );
    len = sizeof( snd_buf );
    state = getsockopt( sock, SOL_SOCKET, SO_SNDBUF, (void*)&snd_buf, &len );
    if ( state )
        error_handling( "getsockopt() error" );

    len = sizeof( rcv_buf );
    state = getsockopt( sock, SOL_SOCKET, SO_RCVBUF, (void*)&rcv_buf, &len );
    if ( state )
        error_handling( "getsockopt() error" );

    printf( "Input buffer size: %d \n", rcv_buf );  // Input buffer size: 131072
    printf( "Output buffer size: %d \n", snd_buf ); // Output buffer size: 16384

    return 0;
}
void error_handling( char* message )
{
    fputs( message, stderr );
    fputc( '\n', stderr );
    exit( EXIT_FAILURE );
}
