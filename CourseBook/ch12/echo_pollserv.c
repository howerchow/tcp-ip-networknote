#include <stddef.h>
#define _GNU_SOURCE
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define POLL_SIZE 1024
int setnonblocking( int fd );

int main( int argc, char* argv[] )
{
    // 创建一个侦听socket
    int listenfd = socket( AF_INET, SOCK_STREAM, 0 );
    if ( listenfd == -1 )
    {
        fprintf( stderr, "create listen socket error.\n" );
        return -1;
    }

    // 将侦听socket设置为非阻塞的
    setnonblocking( listenfd );

    // 复用地址和端口号
    int on = 1;
    setsockopt( listenfd, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof( on ) );
    setsockopt( listenfd, SOL_SOCKET, SO_REUSEPORT, (char*)&on, sizeof( on ) );

    // 初始化服务器地址
    struct sockaddr_in bindaddr;
    bindaddr.sin_family = AF_INET;
    bindaddr.sin_addr.s_addr = htonl( INADDR_ANY );
    bindaddr.sin_port = htons( 9190 );

    if ( bind( listenfd, (struct sockaddr*)&bindaddr, sizeof( bindaddr ) ) == -1 )
    {
        fprintf( stderr, "bind listen socket error.\n" );
        close( listenfd );
        exit( EXIT_FAILURE );
    }

    // 启动侦听
    if ( listen( listenfd, SOMAXCONN ) == -1 )
    {
        fprintf( stderr, "listen error.\n" );
        close( listenfd );
        return -1;
    }

    struct pollfd fds[ POLL_SIZE ] = {};
    fds[ 0 ].fd = listenfd;
    fds[ 0 ].events = POLLIN;
    fds[ 0 ].revents = 0;

    int max_fd = listenfd + 1;

    // 是否存在无效的fd标志
    bool exist_invalid_fd;
    for ( ;; )
    {
        exist_invalid_fd = false;
        int n = poll( fds, max_fd + 1, 1000 );
        if ( n < 0 )
        {
            // 被信号中断
            if ( errno == EINTR )
                continue;

            // 出错，退出
            break;
        }
        else if ( n == 0 )
        {
            // 超时，继续
            continue;
        }

        for ( size_t i = 0; i < POLL_SIZE; ++i )
        {
            // 事件可读
            if ( fds[ i ].revents & POLLIN )
            {
                if ( fds[ i ].fd == listenfd )
                {
                    // 侦听socket，接受新连接
                    struct sockaddr_in clientaddr;
                    socklen_t clientaddrlen = sizeof( clientaddr );
                    // 接受客户端连接, 并加入到fds集合中
                    int clientfd = accept( listenfd, (struct sockaddr*)&clientaddr, &clientaddrlen );
                    if ( clientfd != -1 )
                    {
                        // 将客户端socket设置为非阻塞的
                        setnonblocking( clientfd );
                        fds[ clientfd ].fd = clientfd;
                        fds[ clientfd ].events = POLLIN;
                        fds[ clientfd ].revents = 0;
                        fprintf( stderr, "new client accepted, clientfd: %d\n", clientfd );
                    }
                }
                else
                {
                    // 普通clientfd,收取数据
                    char buf[ 64 ] = { 0 };
                    int m = recv( fds[ i ].fd, buf, 64, 0 );
                    if ( m <= 0 )
                    {
                        if ( errno != EINTR && errno != EWOULDBLOCK )
                        {
                            close( fds[ i ].fd );
                            break;
                        }
                    }
                    else
                    {
                        fprintf( stdout, "recv from client: %s, clientfd: %d\n", buf, fds[ i ].fd );
                        send( i, buf, m, 0 );
                    }
                }
            }
            else if ( fds[ i ].revents & POLLERR )
            {
                // TODO: 暂且不处理
            }
        }
    }

    for ( size_t i = 0; i < POLL_SIZE; ++i )
    {
        close( fds[ i ].fd );
    }

    return 0;
}

int setnonblocking( int fd )
{
#ifdef _WIN32
    {
        unsigned long nonblocking = 1;
        if ( ioctlsocket( fd, FIONBIO, &nonblocking ) == SOCKET_ERROR )
        {
            event_sock_warn( fd, "ioctlsocket(%d, FIONBIO, &%lu)", (int)fd, (unsigned long)nonblocking );
            return -1;
        }
    }
#else
    {
        int flags;
        if ( ( flags = fcntl( fd, F_GETFL, NULL ) ) < 0 )
        {
            fprintf( stderr, "fcntl(%d, F_GETFL)\n", fd );
            return -1;
        }
        if ( !( flags & O_NONBLOCK ) )
        {
            if ( fcntl( fd, F_SETFL, flags | O_NONBLOCK ) == -1 )
            {
                fprintf( stderr, "fcntl(%d, F_SETFL)", fd );
                return -1;
            }
        }
    }
#endif
    return 0;
}
