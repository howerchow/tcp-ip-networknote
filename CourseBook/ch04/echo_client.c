#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define BUF_SIZE 1024
void error_handling( char* message );

int main( int argc, char* argv[] )
{
    if ( argc != 3 )
    {
        printf( "Usage : %s <IP> <port>\n", argv[ 0 ] );
        exit( EXIT_FAILURE );
    }

    int sock;
    char message[ BUF_SIZE ];
    int str_len;
    struct sockaddr_in serv_adr;

    sock = socket( AF_INET, SOCK_STREAM, 0 );
    if ( sock == -1 )
        error_handling( "socket() error" );

    memset( &serv_adr, 0, sizeof( serv_adr ) );
    serv_adr.sin_family = AF_INET;
    inet_pton( AF_INET, argv[ 1 ], &serv_adr.sin_addr );
    serv_adr.sin_port = htons( atoi( argv[ 2 ] ) );

    if ( connect( sock, (struct sockaddr*)&serv_adr, sizeof( serv_adr ) ) == -1 )
        error_handling( "connect() error!" );
    else
        puts( "Connected..........." );

    for ( ;; )
    {
        fputs( "Input message(Q to quit): ", stdout );
        fgets( message, BUF_SIZE, stdin );

        if ( !strcmp( message, "q\n" ) || !strcmp( message, "Q\n" ) )
            break;

        write( sock, message, strlen( message ) );
        str_len = read( sock, message, BUF_SIZE - 1 );
        message[ str_len ] = 0;
        printf( "Message from server: %s", message );
    }
    close( sock );
    return 0;
}

void error_handling( char* message )
{
    fputs( message, stderr );
    fputc( '\n', stderr );
    exit( EXIT_FAILURE );
}
