#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define BUF_SIZE 30
void error_handling( char* message );

int main( int argc, char* argv[] )
{
    if ( argc != 3 )
    {
        printf( "Usage : %s <IP> <port>\n", argv[ 0 ] );
        exit( EXIT_FAILURE );
    }

    int sock;
    char message[ BUF_SIZE ];
    int str_len;
    socklen_t adr_sz;

    struct sockaddr_in serv_adr, from_adr;

    // 创建 UDP 套接字
    sock = socket( AF_INET, SOCK_DGRAM, 0 );
    if ( sock == -1 )
        error_handling( "socket() error" );

    memset( &serv_adr, 0, sizeof( serv_adr ) );
    serv_adr.sin_family = AF_INET;
    inet_pton( AF_INET, argv[ 1 ], &serv_adr.sin_addr );
    serv_adr.sin_port = htons( atoi( argv[ 2 ] ) );

    for ( ;; )
    {
        fputs( "Insert message(q to quit): ", stdout );
        fgets( message, sizeof( message ), stdin );
        if ( !strcmp( message, "q\n" ) || !strcmp( message, "Q\n" ) )
            break;
        // 向服务器传输数据,会自动给自己分配IP地址和端口号
        sendto( sock, message, strlen( message ), 0, (struct sockaddr*)&serv_adr, sizeof( serv_adr ) );
        adr_sz = sizeof( from_adr );
        str_len = recvfrom( sock, message, BUF_SIZE, 0, (struct sockaddr*)&from_adr, &adr_sz );
        message[ str_len ] = 0;
        printf( "Message from server: %s", message );
    }
    close( sock );
    return 0;
}

void error_handling( char* message )
{
    fputs( message, stderr );
    fputc( '\n', stderr );
    exit( EXIT_FAILURE );
}
