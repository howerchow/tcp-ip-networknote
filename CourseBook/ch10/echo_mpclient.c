#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define BUF_SIZE 30
void error_handling( char* message );
void read_routine( int sock, char* buf );
void write_routine( int sock, char* buf );

int main( int argc, char* argv[] )
{
    if ( argc != 3 )
    {
        printf( "Usage : %s <IP> <port>\n", argv[ 0 ] );
        exit( EXIT_FAILURE );
    }

    int sock;
    pid_t pid;
    char buf[ BUF_SIZE ];
    struct sockaddr_in serv_adr;

    sock = socket( AF_INET, SOCK_STREAM, 0 );

    memset( &serv_adr, 0, sizeof( serv_adr ) );
    serv_adr.sin_family = AF_INET;
    inet_pton( AF_INET, argv[ 1 ], &serv_adr.sin_addr );
    serv_adr.sin_port = htons( atoi( argv[ 2 ] ) );

    if ( connect( sock, (struct sockaddr*)&serv_adr, sizeof( serv_adr ) ) == -1 )
        error_handling( "connect() error!" );

    pid = fork();
    if ( pid == 0 )
        write_routine( sock, buf );
    else
        read_routine( sock, buf );

    close( sock );
    return 0;
}

void read_routine( int sock, char* buf )
{
    for ( ;; )
    {
        int str_len = read( sock, buf, BUF_SIZE );
        if ( str_len == 0 )
            return;

        buf[ str_len ] = 0;
        printf( "Message from server: %s", buf );
    }
}

void write_routine( int sock, char* buf )
{
    for ( ;; )
    {
        fgets( buf, BUF_SIZE, stdin );
        if ( !strcmp( buf, "q\n" ) || !strcmp( buf, "Q\n" ) )
        {
            /* 向服务器端传递 EOF,因为fork函数复制了文件描述度，所以通过1次close调用不够 */
            shutdown( sock, SHUT_WR );
            return;
        }
        write( sock, buf, strlen( buf ) );
    }
}

void error_handling( char* message )
{
    fputs( message, stderr );
    fputc( '\n', stderr );
    exit( EXIT_FAILURE );
}
