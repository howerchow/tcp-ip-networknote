#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define BUF_SIZE 100
#define NAME_SIZE 20

void* send_msg( void* arg );
void* recv_msg( void* arg );
void error_handling( char* msg );

char name[ NAME_SIZE ] = "[DEFAULT]";
char msg[ BUF_SIZE ] = {};

int main( int argc, char* argv[] )
{
    if ( argc != 4 )
    {
        printf( "Usage : %s <IP> <port> <name>\n", argv[ 0 ] );
        exit( EXIT_FAILURE );
    }

    int sock;
    struct sockaddr_in serv_addr;
    pthread_t snd_thread, rcv_thread;
    void* thread_return;

    sprintf( name, "[%s]", argv[ 3 ] );
    sock = socket( AF_INET, SOCK_STREAM, 0 );

    memset( &serv_addr, 0, sizeof( serv_addr ) );
    serv_addr.sin_family = AF_INET;
    inet_pton( AF_INET, argv[ 1 ], &serv_addr.sin_addr );
    serv_addr.sin_port = htons( atoi( argv[ 2 ] ) );

    if ( connect( sock, (struct sockaddr*)&serv_addr, sizeof( serv_addr ) ) == -1 )
        error_handling( "connect() error" );

    pthread_create( &snd_thread, NULL, send_msg, (void*)&sock ); // 创建发送消息线程
    pthread_create( &rcv_thread, NULL, recv_msg, (void*)&sock ); // 创建接受消息线程
    pthread_join( snd_thread, &thread_return );
    pthread_join( rcv_thread, &thread_return );
    close( sock );
    return 0;
}

void* send_msg( void* arg ) // 发送消息
{
    int sock = *( (int*)arg );
    char name_msg[ NAME_SIZE + BUF_SIZE ] = {};
    for ( ;; )
    {
        fgets( msg, BUF_SIZE, stdin );
        if ( !strcmp( msg, "q\n" ) || !strcmp( msg, "Q\n" ) )
        {
            close( sock );
            exit( 0 );
        }
        sprintf( name_msg, "%s %s", name, msg );
        write( sock, name_msg, strlen( name_msg ) );
    }
    return NULL;
}

void* recv_msg( void* arg ) // 读取消息
{
    int sock = *( (int*)arg );
    char name_msg[ NAME_SIZE + BUF_SIZE ] = {};
    int str_len;
    for ( ;; )
    {
        str_len = read( sock, name_msg, NAME_SIZE + BUF_SIZE - 1 );
        if ( str_len == -1 )
            return (void*)-1;
        name_msg[ str_len ] = 0;
        fputs( name_msg, stdout );
    }
    return NULL;
}

void error_handling( char* msg )
{
    fputs( msg, stderr );
    fputc( '\n', stderr );
    exit( EXIT_FAILURE );
}
